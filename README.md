# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado?
```plantuml
@startmindmap
+  Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? 
++ ¿Qué es un sistema antiguo?
+++_ es
++++ Un sistema son aquellos ordenadores que ya no haya salido a la venta o que ya no requiera alguna actualización.
++ ¿Que es un sistema actual?
+++_ son
++++ Todos aquellos ordenadores con los que contamos en casa que aun se puedan actualizar por si solor.
++ Velocidad 
+++_ sistema antiguo 
++++ Se media en Megahercios (MHz) es una unidad de medida de la frecuencia, que equivale a 106 hercios (1 millón)
+++++ se tenia menos potencia 
+++++ se tenia limitacion de recursos
+++_ sistema actual
++++ Se media en Gigahercios (GHz) hablamos de 109 millones de Hercios.
+++++_ se transformo en
++++++ Memorias mucho mas potentes. 
++++++ Incremento de potencia
++++++ Ya no cuenta con tantas debilidades de recursos. 
++ Programas y juegos 
+++_ Actualidad 
++++ Presentan fllas 
++++ Cuentan con actualizaciones constantes.
+++_ Antiguedad 
++++ Tu tenias el control de cuanto se tardaria
++ Tipo de programacion.
+++_ Actualidad. 
++++ Se preocupa por la interfaz en lugar del trabajo para realiar programas multiplataforma.
++++ Utiliza un lenguaje que deuestra como piensa un programador 
++++ Ocupa lenguaje de alto nivel. 
++ Lenguaje que ocupa.
+++_ Actualidad 
++++ Lenguaje de alto nivel.
+++++_ Ventajas 
++++++ El programador ya no conoce a fondo la maquina.
++++++ Permite la programacion a nivel abstracto.
+++++_ Desventajas
++++++ Ocurre una mayor sobrecarga.
++++++ Con el timpo afecta el CPU.
+++_ Antiguedad
++++ Ensamblador.
+++++_ Ventajas 
++++++ Se tiene un control total de la ejecucion.
++++++ El programador conoce el tiempo de respuesta.
+++++_ Desventajas
++++++ Se tiene que conocer a fondo cada maquina.
++++++ La programacion es distinta.
@endmindmap
```
# Hª de los algoritmos y de los lenguajes de programación (2010)
```plantuml
@startmindmap
+ _Hª de los algoritmos y de los lenguajes de programación (2010)
++ ¿Qué es un Algoritmo?
+++_ es
++++ Un algoritmo es una lista bien ordenada y definida de operaciones que permite encontrar la solucion a un problema.
++ ¿Cual es la importancia del algoritmo?
+++_ es 
++++ Llevar acabo procesos y resolver problemas matematicos o de otro tipo.
++ La evolucion del algoritmo  
+++_ comenzo desde
++++ La antigua Mesopotamia con los algoritmoz creador para describir los calculos.
++++ En el siglo XVII se creo la primera calculadora del bolsillo. 
++++ En el siglo XX llegaron las maquinas programables.
++ ¿Qué es informática? 
+++ manejo de procesadores de textos y hojas de calculo.  
++ ¿Qué es la ingeniería informática? 
+++ Es la disciplina que enseña a construir todo esto, maquinas que ejecutan programas y lenguajes de programacion .
++ Relacion de los algoritmos con la informática.
+++ los algoritmos son procedimientos que una maquina puede realizar, la informática aporta maquinas muy rápidas que ejecutan estos algoritmos listos para ser ejecutados, se dice que la maquina nació del algoritmo.  
++ Tipos de algoritmos.
+++_ son
++++ Razonables 
++++ No razonables.
++ Paradigma de los lenguajes de Programación
+++ Tienen una perspectiva de como realizar los cálculos.
+++_ se dividen en 
++++ Imperativo: secuancia de ordenes 
++++ Funcional: es muy simplificado.
++++ Orientado a Objetos: comunicacion con lenguajes.
++++ Logica: obtiminazcion. 
 
@endmindmap
```
# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
```plantuml
@startmindmap
* _ Lenguajes de Programación y Sistemas Informáticos. 
++ Los ordenadores 
+++ Fueron creados para facilitar el trabajo. 
++ Los lenguajes de programacion 
+++_ son
++++ Quieres facilitan el trabajo intelectual, buscan constantemnete la innovacion.
++++ Buscan resolver problemas 
++++ Buscan ser eficientes.
++ ¿Que es un paradigma? 
+++ Es una forma de aproximarse a la solucion de un problema, siendo esta la manera en la que se puede dar una solucion
++ Tipos de paradigmas.
+++ Funcional
++++ Su base es lenguaje de matematicas
++++ Usa recursividad 
+++++ Programas mas ocupados.
++++++ ML
++++++ HOPE
+++ Logico
++++ Predicados logicos 
++++ No se tiene ni numeros ni letras 
+++++ Lenguaje que ocupa.
++++++ Prolog
+++ Programaccion Concurrente 
++++ cuenta con sistemas automaticos.
+++++ Algunos usuarios buscan acceder al sitio.
+++ Programacion distribuida
++++ Es una comunicacion entre ordenadores.
++++ Programas divididos en ordenadores 
+++ Prograciacion orientada en componentes
++++ Permite reutilizacion 
++++ Conjunto de objetos.
+++ Programacion orientada en aspectos 
++++ Agrega capaz base segun las necesidades del programa.
++++ Las capas deben de ser visibles.
+++ Programacion orientada a software.
++++ Agentes que forman parte de la programacion  

@endmindmap
```